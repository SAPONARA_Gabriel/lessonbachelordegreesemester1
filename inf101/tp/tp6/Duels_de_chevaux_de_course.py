import math
import copy

def minimum(liste):
    minimum = liste[0]
    for elem in liste :
        if elem < minimum :
            minimum = elem
    return minimum



def maximum(liste):
    maximum = liste[0]
    for elem in liste :
        if elem > maximum :
            maximum = elem
    return maximum


def plus_proche (liste):
    ensChevaux = [0]*2*len(liste)
    index = 0
    diff = maximum(liste) - minimum(liste)
    for elem1 in enumerate(liste) :
        chevauxProches = []
        chevauxProches.append(elem1[1])
        diff1 = diff
        cheval = []
        for elem2 in enumerate(liste):
            if elem1[0] != elem2[0] :
                diff2 = math.fabs(elem1[1] - elem2[1])
                if diff2 < diff1 :
                    diff1 = diff2
                    cheval[:] = []
                    cheval.append(elem2[1])
                elif diff2 == diff1 :
                    cheval.append(elem2[1])
        chevauxProches.extend(cheval)
        ensChevaux[index] = diff1
        index = index + 1
        ensChevaux[index] = copy.deepcopy(chevauxProches)
        index = index + 1
        
    return ensChevaux



def tri_insertion_v2(liste):
    liste_triee = []
    liste_triee.append(liste[0])
    indice = 1
    while indice < len(liste):
        index_min = 0
        index_max = len(liste_triee) - 1
        while index_min != index_max :
            index_median = (index_min + index_max)//2
            if liste[indice] > liste_triee[index_median] :
                index_min = index_median + 1
            elif liste[indice] < liste_triee[index_median] :
                index_max = index_median
            else :
                index_min = index_median
                index_max = index_median
        if (index_min == len(liste_triee) - 1) and (liste[indice] > liste_triee[index_min]) : # l'élément à insérrer est supérieur au dernier élément de la liste triée
            liste_triee.append(liste[indice])
        else :
            liste_triee.insert(index_min,liste[indice])
        indice = indice + 1

    return liste_triee



def couples_course(liste):
    liste_triee = tri_insertion_v2(liste)
    liste_couple = [0] * (len(liste)//2)
    index = 0
    while len(liste_triee) > 0 :
        couple = []
        while len(couple) < 2 :
            couple.append(liste_triee[0])
            liste_triee.remove(liste_triee[0])
        liste_couple[index] = couple
        index = index + 1
    
    return liste_couple



if __name__ == "__main__" :
    l1 = [25,18,18,65,48,75,25,3]
    print(plus_proche(l1))
    print(couples_course(l1))
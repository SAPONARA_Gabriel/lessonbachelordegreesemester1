import copy

def minimum(liste):
    minimum = liste[0]
    for elem in liste :
        if elem < minimum :
            minimum = elem
    return minimum


def tri_selection(liste):
    liste_a_triee = copy.deepcopy(liste)
    liste_triee = []
    while len(liste_a_triee) > 0 :
        liste_triee.append(minimum(liste_a_triee))
        liste_a_triee.remove(minimum(liste_a_triee))
    return liste_triee


def tri_insertion(liste):
    liste_triee = []
    liste_triee.append(liste[0])
    index = 1
    while index < len(liste):
        indice = 0
        if liste[index] < liste_triee[indice] :
            liste_triee.insert(0,liste[index])
        elif liste[index] == liste_triee[indice] :
            liste_triee.append(liste[index])
        else :
            while (indice < len(liste_triee)) and (liste[index] > liste_triee[indice]):
                indice = indice + 1
            liste_triee.insert(indice,liste[index])
        index = index + 1
    return liste_triee


def tri_insertion_v2(liste):
    liste_triee = []
    liste_triee.append(liste[0])
    indice = 1
    while indice < len(liste):
        index_min = 0
        index_max = len(liste_triee) - 1
        while index_min != index_max :
            index_median = (index_min + index_max)//2
            if liste[indice] > liste_triee[index_median] :
                index_min = index_median + 1
            elif liste[indice] < liste_triee[index_median] :
                index_max = index_median
            else :
                index_min = index_median
                index_max = index_median
        if (index_min == len(liste_triee) - 1) and (liste[indice] > liste_triee[index_min]) : # l'élément à insérrer est supérieur au dernier élément de la liste triée
            liste_triee.append(liste[indice])
        else :
            liste_triee.insert(index_min,liste[indice])
        indice = indice + 1

    return liste_triee

if __name__ == "__main__" :
    l1 = [15,2,5,20,7,-3,48,20,-1]
    print("\t Test de la fonction tri_insertion avec la liste l1 :")
    print("\n")
    print(l1)
    print(tri_insertion(l1))
    print("\n")
    print("\t Test de la fonction tri_selection avec la liste l1 :")
    print("\n")
    print(l1)
    print(tri_selection(l1))
    print("\n")
    print("\t Test de la fonction tri_insertion_v2 avec la liste l1 :")
    print("\n")
    print(l1)
    print(tri_insertion_v2(l1))

import math

def proche_zero(entiers):
    if len(entiers) == 0 :
        return None
    temp1 = entiers[0]
    index = 1
    while index < len(entiers):
        if math.fabs(entiers[index]) < math.fabs(temp1) :
            temp1 = entiers[index]
        elif (math.fabs(entiers[index]) == math.fabs(temp1)) and entiers[index] > 0:
            temp1 = entiers[index]
        index = index + 1
    
    return temp1

if __name__ == "__main__" :
    l2 = []
    l1 = [58,-1,3,5,1,1,78,-1,-2]
    print(proche_zero(l1))
    print(proche_zero(l2))

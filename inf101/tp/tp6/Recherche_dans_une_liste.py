import copy

def minimum(liste):
    minimum = liste[0]
    for elem in liste :
        if elem < minimum :
            minimum = elem
    return minimum


def contient(liste, elem):
    for elem2 in liste :
        if elem2 == elem :
            return True
    return False


def minimum2(liste):
    listeCopie = copy.deepcopy(liste)
    minListe = minimum(liste)
    while minimum(listeCopie) == minListe and len(listeCopie) > 0 :
        listeCopie.remove(minimum(listeCopie))
        if len(listeCopie) == 0:
            return None
    return minimum(listeCopie)



def maximum(liste):
    maximum = liste[0]
    for elem in liste :
        if elem > maximum :
            maximum = elem
    return maximum



if __name__ == "__main__":
    L1 = [-1,3.5,4]
    print(minimum2(L1))
    print(contient(L1,5),contient(L1,3.5))
    print(minimum(L1),maximum(L1))
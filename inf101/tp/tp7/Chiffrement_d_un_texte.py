#coding:utf-8

def position_lettre(lettre):
    if lettre.isupper() == True:
        position = ord(lettre)-64
        return position
    else :
        position = ord(lettre)-96
        return position



def mot_code(mot):
    mot_code = []
    for lettre in mot :
        mot_code.append(str(position_lettre(lettre)))
    mot_code = "+".join(mot_code)
    return mot_code


def texte_code(texte):
    texte_bis = []
    carac_ponc = [" ",",",".","!","?",";",":"]
    mot = ""
    for carac in texte :
        if carac not in carac_ponc :
            mot = mot + carac
        else :
            texte_bis.append(mot)
            texte_bis.append(carac)
            mot = ""
    
    texte_coder = []
    
    for elem in texte_bis :
        if elem not in carac_ponc :
            texte_coder.append(mot_code(elem))
        else :
            texte_coder.append(elem)
    
    texte_coder = "".join(texte_coder)

    return texte_coder



if __name__ == "__main__":
    texte_a_coder = str(input("""Quel est votre texte à coder ?"""))
    print(texte_a_coder)
    print("\t Voici votre texte codé :",texte_code(texte_a_coder))
def position_lettre(lettre):
    if lettre.isupper() == True:
        position = ord(lettre)-64
        return position
    else :
        position = ord(lettre)-96
        return position
    


if __name__ == "__main__" :
    lettre = 'a'
    lettre2 = 'A'
    lettre3 = 'z'
    lettre4 = 'E'
    print(position_lettre(lettre),position_lettre(lettre2),position_lettre(lettre3),position_lettre(lettre4))

import statistics

def taille_mots_texte(texte):
    ens_mots = texte.split(" ")
    long_mots = []
    for mot in ens_mots :
        long_mots.append(len(mot))
    
    return long_mots




def maximum_long(texte):
    ens_long_mots = taille_mots_texte(texte)
    max = 0
    for long in ens_long_mots :
        if long > max :
            max = long
    
    return max




def maximum_long_mot (texte):
    ens_mots = texte.split(" ")
    ens_long_mots = taille_mots_texte(texte)
    max_long = maximum_long(texte)
    index = 0
    indice = 0
    while index < len(ens_long_mots) :
        if ens_long_mots[index] > max_long :
            indice = index
        index = index + 1
    
    return ens_mots[indice]




def affiche_long_ens_mots(texte,liste_long):
    ens_mots = texte.split(" ")
    index = 0
    long_max = maximum_long(texte)
    while index < len(ens_mots) :
        if len(ens_mots[index]) < long_max :
            print(ens_mots[index]," "*(long_max - len(ens_mots[index]) - 1)," : ",liste_long[index]," caractères")
        else :
            print(ens_mots[index]," : ",liste_long[index]," caractères")
        index = index + 1



def mots_plus_long_moy(texte):
    ens_mots = texte.split(" ")
    ens_long_mots = taille_mots_texte(texte)
    ens_mots_plus_moy = []
    moyenne = round(statistics.mean(ens_long_mots),0)
    index = 0
    while index < len(ens_long_mots) :
        if ens_long_mots[index] > moyenne :
            ens_mots_plus_moy.append(ens_mots[index])
            ens_mots_plus_moy.append(ens_long_mots[index]) # on met aussi la longueur du mots dans l'ensemble pour l'affichage
        index = index + 1

    return ens_mots_plus_moy



def affiche_mots_plus_moy(ens_mots_plus_moy,texte) :
    index = 0
    long_max = maximum_long(texte)
    while index < len(ens_mots_plus_moy) :
        if len(ens_mots_plus_moy[index]) < long_max :
            print(ens_mots_plus_moy[index]," "*(long_max - len(ens_mots_plus_moy[index]) - 1)," : ",ens_mots_plus_moy[index + 1]," caractères")
            index = index +2
        else :
            print(ens_mots_plus_moy[index]," : ",ens_mots_plus_moy[index + 1]," caractères")
            index = index +2

#coding:utf-8

import random, statistics
import matplotlib.pyplot as plt

def initialisation(distance_max):
    liste_dist = []
    for i in range (1,366):
        liste_dist.append(round(random.uniform(0,distance_max),2))

    return liste_dist


def moyenne_jour_semaine(liste_distance,num_jour):
    index = num_jour - 1
    distance_jour = []
    for jour in range (index,len(liste_distance),7):
        distance_jour.append(liste_distance[jour])
    
    return round(statistics.mean(distance_jour),3)
    

def moyenne_jour_semaine_bis(liste_distance,num_jour,debut_annee):
    if num_jour >= debut_annee :
        index = num_jour - debut_annee
    else : # num_jour < debut_annee
        index = num_jour + 7 - debut_annee
    
    distance_jour = []
    for jour in range (index,len(liste_distance),7):
        distance_jour.append(liste_distance[jour])
    
    return round(statistics.mean(distance_jour),3)



def moyenne_chaque_jour (liste_distance,debut_annee) :
    liste_moyenne = []
    for numero_jour in range (1,8) :
        liste_moyenne.append(moyenne_jour_semaine_bis(liste_distance,numero_jour,debut_annee))
    
    return liste_moyenne


def trace_histogramme(liste_distance):
    plt.axis([0,100,0,10])
    plt.xlabel('Distance parcourue')
    plt.ylabel('Fréquence de la distance')
    plt.hist(liste_distance, len(liste_distance), facecolor = 'b', alpha = 1 )
    plt.grid(True)
    plt.show()
    



if __name__ == "__main__" :
    liste_distance1 = initialisation(100)
    print(liste_distance1)
    print(moyenne_jour_semaine(liste_distance1,2))
    print(moyenne_jour_semaine_bis(liste_distance1,2,4))
    print(moyenne_chaque_jour(liste_distance1,4))
    trace_histogramme(liste_distance1)
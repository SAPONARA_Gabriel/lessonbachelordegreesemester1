import comptage_et_statistiques as ces
import statistics , random


if __name__ == "__main__":
    print("""Ce programe vous indique la longueur de chaque mots de votre texte, le mot le plus long et l'ensemble des mots qui ont une longueur supérieur à la longueur moyenne des mots du texte.\n Attention votre texte ne doit comporter que des caractères et des espaces""")
    texte = str(input("Quel est votre texte ?"))
    print("Voici la longueur de chaque mot : \n")
    taille_mots = ces.taille_mots_texte(texte)
    affiche = ces.affiche_long_ens_mots(texte,taille_mots)
    print("Le mot le plus long du texte est :",ces.maximum_long_mot(texte),". Il a :",ces.maximum_long(texte)," caractères.")
    print("La longueur moyenne des mots du texte est de : ", round(statistics.mean(ces.taille_mots_texte(texte)),0) ,".")
    print("Voici les mots plus longs que la longueur moyenne des mots du texte :")
    affiche_mots_moy = ces.affiche_mots_plus_moy(ces.mots_plus_long_moy(texte),texte)
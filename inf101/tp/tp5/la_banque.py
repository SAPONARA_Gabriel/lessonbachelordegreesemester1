#coding:utf-8

def capital (nb_annees, capital_debut):
    if nb_annees == 0:
        return capital_debut
    return capital(nb_annees-1,capital_debut)*1.05 - 11


def gagne_argent(nb_annees,capital_debut):
    return capital(nb_annees, capital_debut) >= capital_debut
    

def placement_min(nb_annees,but):
    if nb_annees == 0:
        return but
    return (placement_min(nb_annees-1,but)+11)/1.05


def duree_min(capital_fin,but):
    if capital_fin <= (11/0.05) :
        erreur = ("""Votre capital est trop petit, il faut un capital supérieur à {} pour gagner de l'argent. Si le capital est égal à {} votre capital restera constant, et sinon il va diminuer.""" .format(11/0.05,11/0.05))
        return erreur
    else:
        nb_annees = 0
        while capital (nb_annees,capital_fin) < but :
            nb_annees = nb_annees + 1
        return nb_annees
        

if __name__ == "__main__" :
    annees = int(input("""Pendant combien d'années voulez-vous laisser votre capital ?"""))
    capital_dep = int(input("""Quel est votre capital de départ"""))
    print(capital(annees,capital_dep),"\n")
    if gagne_argent(annees,capital_dep) == True :
        print("Vous avez gagné de l'argent !!! \n")
    else :
        print("Vous avez perdu de l'argent. \n")
    capital_a_atteindre = int(input("""Quel est le montant du capital que vous voulez atteindre ?"""))
    print(placement_min(annees,capital_a_atteindre),"\n")
    print("Vous devez économisé pendant: {} ans, pour atteindre {}, avec un capital de : {}".format(duree_min(capital_dep,capital_a_atteindre),capital_a_atteindre,capital_dep))
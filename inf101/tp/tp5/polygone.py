import turtle

def polygone(nb_cote,cote):
    angle = (360//nb_cote)    
    while nb_cote > 0 :
        turtle.forward(cote)
        turtle.right(angle)
        nb_cote = nb_cote - 1

import turtle
import math

def diametre_polygone(nb_cotes,cote):
    return cote/(math.sin(math.pi/nb_cotes))

def aller_sans_tracer (x,y):
    turtle.up()
    turtle.goto(x,y)
    turtle.down()

def descendre_sans_tracer (longueur):
    turtle.up()
    turtle.right(90)
    turtle.forward(longueur)
    turtle.left(90)
    turtle.down()


def polygone(nb_cote,cote):
    angle = (360//nb_cote)    
    while nb_cote > 0 :
        turtle.forward(cote)
        turtle.right(angle)
        nb_cote = nb_cote - 1
        

def colonne_polygone(nb_poly,cote):
    nb_cote = 3
    while nb_poly > 0 :
        polygone(nb_cote,cote)
        nb_poly = nb_poly - 1
        nb_cote = nb_cote + 1
        descendre_sans_tracer(diametre_polygone(nb_cote,cote)+5)
    return nb_cote


def carre (cote):
    nb_cote = 0
    while nb_cote < 4 :
        turtle.forward(cote)
        turtle.right(90)
        nb_cote = nb_cote + 1

def carres_imbriques(cote_debut,nb_carres):
    while nb_carres > 0 :
        carre(cote_debut)
        cote_debut = cote_debut * (2/3)
        nb_carres = nb_carres - 1


def pavage(nb_poly,nb_col,cote):
    aller_sans_tracer (-270,330)
    nb_colonne = 1
    while nb_col > 0 :
        nb_cote = colonne_polygone(nb_poly,cote)
        if nb_col == 1:
            carres_imbriques(100,5)
        aller_sans_tracer(-270+diametre_polygone(nb_cote,cote)*nb_colonne,330)
        nb_colonne = nb_colonne +1
        cote = cote + 10
        nb_col = nb_col-1



if __name__ == "__main__":
    nb_poly = int(input("""Combien voulez-vous de polygones par colonne pour votre pavage ?"""))
    nb_col = int(input("""Combien voulez-vous de colonnes pour votre pavage ?"""))
    cote = int(input("""Quelle longueur souhaitez-vous pour les côtés des premières figures ?"""))
    pavage(nb_poly,nb_col,cote)



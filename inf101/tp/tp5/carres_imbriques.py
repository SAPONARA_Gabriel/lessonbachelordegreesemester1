import turtle

def carre (cote):
    nb_cote = 0
    while nb_cote < 4 :
        turtle.forward(cote)
        turtle.right(90)
        nb_cote = nb_cote + 1

def carres_imbriques(cote_debut,nb_carres):
    while nb_carres > 0 :
        carre(cote_debut)
        cote_debut = cote_debut * (2/3)
        nb_carres = nb_carres - 1

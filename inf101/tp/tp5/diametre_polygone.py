import turtle
import math

def diametre_polygone(nb_cotes,cote):
    return cote/(math.sin(math.pi/nb_cotes))

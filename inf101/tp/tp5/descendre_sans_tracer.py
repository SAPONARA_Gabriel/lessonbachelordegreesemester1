import turtle

def descendre_sans_tracer (longueur):
    turtle.up()
    turtle.right(90)
    turtle.forward(longueur)
    turtle.left(90)
    turtle.down()

def natalite_poules(nb_poules_init,nat_poules,pas_de_temps):
    temps = 0
    while temps < pas_de_temps :
        new_poules = nat_poules*nb_poules_init
        nb_poules_init = nb_poules_init + new_poules
        temps = temps + 1
    return nb_poules_init


def mortalite_poule(nb_poules_init,nat_poules,mort_poules,pas_de_temps):
    temps = 0
    while temps < pas_de_temps :
        new_poules = nat_poules*nb_poules_init
        dead_poules = mort_poules*nb_poules_init
        nb_poules_init = nb_poules_init + new_poules - dead_poules
        temps = temps + 1
    return nb_poules_init

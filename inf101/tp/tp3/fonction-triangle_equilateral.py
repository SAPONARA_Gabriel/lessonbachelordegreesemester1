def triangle_equilateral (x,y,cote):
    import turtle
    turtle.up()
    turtle.goto(x,y)
    turtle.down()
    nb_cote=0
    while nb_cote<3:
        turtle.forward(cote)
        turtle.right(120)
        nb_cote=nb_cote+1

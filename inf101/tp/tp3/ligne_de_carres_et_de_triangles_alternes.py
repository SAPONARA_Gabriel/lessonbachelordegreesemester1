import turtle

turtle.up()
turtle.goto(-200,200)
turtle.down()

nb_figures=int(input("""Combien de figures (carrés et triangles) souhaiteriez-vous voir tracées ?"""))

nb_fig=0

while nb_fig<nb_figures:
    nb_cote=0
    while nb_cote<4:
        turtle.forward(50)
        turtle.right(90)
        nb_cote=nb_cote+1
    nb_fig=nb_fig+1
    turtle.up()
    turtle.forward(60)
    turtle.down()
    if nb_fig==nb_figures:
        break
    nb_cot=0
    while nb_cot<3:
        turtle.forward(50)
        turtle.right(120)
        nb_cot=nb_cot+1
    nb_fig=nb_fig+1
    turtle.up()
    turtle.forward(60)
    turtle.down()

import turtle

def rosace (x,y,nb,r):
    turtle.up()
    turtle.goto(x,y)
    turtle.down()
    nb_cercle=0
    while nb_cercle<nb:
        turtle.circle(r)
        nb_cercle=nb_cercle+1
        turtle.right(360//nb)

def pavage_rosace(nb_rosaces_lignes,nb_lignes):
    nombre_lg=0
    x=int(input("""Quelle est l'abscisse du centre de votre première rosace ?"""))
    y=int(input("""Quelle est l'ordonnée du centre de votre première rosace ?"""))
    nb=int(input("""Combien voulez-vous de cercles dans vos rosaces ?"""))
    r=int(input("""Quel rayon devrait avoir les cercles de vos rosaces ?"""))
    x_1=x
    while nombre_lg < nb_lignes:
        nb_rosace=0
        x=x_1
        while nb_rosace < nb_rosaces_lignes:
            rosace(x,y,nb,r)
            nb_rosace=nb_rosace+1
            turtle.up()
            turtle.forward(4*r)
            turtle.down()
            x=x+4*r
        turtle.up()
        turtle.goto(x,y-(4*r))
        turtle.down()
        y=y-(4*r)
        nombre_lg=nombre_lg+1

    

if __name__ == "__main__":
    nb_rosaces_lignes=int(input("""Combien voudriez-vous de rosaces par lignes ?"""))
    nb_lignes=int(input("""Combien souhaiteriez-vous de lignes à votre pavage de rosace ?"""))
    pavage_rosace(nb_rosaces_lignes,nb_lignes)
    

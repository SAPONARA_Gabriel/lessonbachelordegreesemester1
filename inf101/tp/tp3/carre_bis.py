import turtle

nb_cote=0

turtle.up()
turtle.goto(-50,20)
turtle.down()

while nb_cote<4:
    turtle.forward(100)
    turtle.right(90)
    nb_cote=nb_cote+1

print(turtle.pos())

import turtle

nb_carre=0
turtle.up()
turtle.goto(-750,375)
turtle.down()

while nb_carre<8:
    nb_cote=0
    while nb_cote<4:
        turtle.forward(50)
        turtle.right(90)
        nb_cote=nb_cote+1
    nb_carre=nb_carre+1
    turtle.up()
    turtle.forward(60)
    turtle.down()

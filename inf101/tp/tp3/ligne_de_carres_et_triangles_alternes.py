import turtle

def carre (cote):
    nb_cote=0
    while nb_cote<4:
        turtle.forward(cote)
        turtle.right(90)
        nb_cote=nb_cote+1


def triangle_equilateral (cote):
    nb_cote=0
    while nb_cote<3:
        turtle.forward(cote)
        turtle.right(120)
        nb_cote=nb_cote+1


if __name__ == "__main__":
    turtle.up()
    turtle.goto(-200,200)
    turtle.down()
    
    nb_figures=int(input("""Combien voulez-vous de figures ?"""))

    nb_fig=0
    longueur=40

    while nb_fig<nb_figures:
        carre(longueur)
        longueur=longueur+15
        nb_fig=nb_fig+1
        if nb_fig==nb_figures:
            break
        turtle.up()
        turtle.forward(longueur+10)
        turtle.down()
        triangle_equilateral(longueur)
        longueur=longueur+15
        nb_fig=nb_fig+1
        turtle.up()
        turtle.forward(longueur+10)
        turtle.down()









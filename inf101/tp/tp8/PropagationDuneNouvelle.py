import random


def initialisationStatut(listeStatut : list) -> list :
    listeStatut[0] = 'C'
    for index in range (1,len(listeStatut)):
        listeStatut[index] = 'I'
    
    return listeStatut


def rencontre (nombrePers) :
    meeting = [random.randint(0,nbPersonne-1),random.randint(0,nbPersonne-1)]
    while meeting[1] == meeting[0]:
        meeting[1] = random.randint(0,nbPersonne-1)
    
    return meeting


def changeStatus(status1 : str, status2 : str ) -> (str,str):
    if status1 == 'C' and status2 == 'C' :
        return ('M','M')
    elif (status1 in ['C','I']) and (status2 in ['C','I']) :
        return ('C','C')
    elif (status1 in ['C','M']) and (status2 in ['C','M']) :
        return ('M','M')
    else :
        return (status1,status2)


def entete (ensStatut : list) -> None :
    print("jour rencontre nature ",end = '')
    for index in range (1,len(ensStatut)+1):
        print("st."+str(index)+" ",end = '')

    print()
    print("0"," "*22, end ='')
    for statut in ensStatut :
        print(statut," "*3, end = '')
    
    print()



def affichage (jour : int, rencontre : list, nature : list, ensStatut : list) -> None :
    print(jour," "*(6*(jour//10)),rencontre[0],","," "*2,rencontre[1]," "*4,nature[0],", ",nature[1]," "*4,sep ='',end = '')
    for statut in ensStatut:
        print(statut," "*3,end = '')
    print("\n")


def simulation (ensStatut : list) -> list :
    entete(ensStatut)
    nbJour = 0
    while 'C' in ensStatut :
        nbJour += 1
        meet = rencontre(len(ensStatut))
        meet2 = [ensStatut[meet[0]],ensStatut[meet[1]]]
        nouveauStatut = changeStatus(meet2[0],meet2[1])
        ensStatut[meet[0]] = nouveauStatut[0]
        ensStatut[meet[1]] = nouveauStatut[1]
        affichage(nbJour,meet,meet2,ensStatut)
    
    return ensStatut




if __name__ == "__main__" :
    nbPersonne = int(input("""Combien y-t-il de personnes lors de cette simulation ? (Il doit y en avoir au moins 2.)"""))
    while nbPersonne < 2 :
        nbPersonne = int(input("""Le nombre de personnes que vous avez saisi est incorrect. Il est inférieur à 2. Veuillez ressaisir le nombre de personnes s'il vous plaît."""))

    statutRencontre = list(range(nbPersonne))
    statutRencontre = initialisationStatut(statutRencontre)

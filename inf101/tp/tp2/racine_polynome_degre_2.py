import math

a = float(input("""Quelle est la valeur du coefficient a ?"""))
b = float(input("""Quelle est la valeur du coefficient b ?"""))
c = float(input("""Quelle est la valeur de c ?"""))

delta = b**2 - 4*a*c

while delta < 0:
    a = float(input("""Quelle est la valeur du coefficient a ?"""))
    b = float(input("""Quelle est la valeur du coefficient b ?"""))
    c = float(input("""Quelle est la valeur de c ?"""))

if delta == 0:
    racine0 = -b/2*a
    print("""La racine du polynôme est égal à :""",racine0)
else:
    racine1 = (-b-(math.sqrt(delta)))/2*a
    racine2 = (-b+(math.sqrt(delta)))/2*a
    print("""Les racines du pôlynomes sont:""", racine1,"""et""",racine2)

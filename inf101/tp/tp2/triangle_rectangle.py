import math

h = float(input("""Quelle est la mesure de l'hypoténuse de votre triangle rectangle ?"""))
a = float(input("""Quelle est la mesure en radians d'un des deux angles non droits ?"""))

adjacent = math.cos(a)*h
oppose = math.sin(a)*h

print("""Les deux petits côtés sont de :,""", adjacent,"""et""", oppose)

if h**2 == adjacent**2 + oppose**2 :
    print("""Les valeurs des petits côtés sont correctes.""")
else:
    print("""Les valeurs trouvées ne vérifient pas le théorème de Pythagore.""")

def nombre_premier (n):
    est_premier = True
    div = 2
    while div*div <= n:
        if n%div == 0 :
            est_premier = False
        div = div + 1
    return est_premier

nb = int(input("""Quel est votre nombre ?"""))

verif = nombre_premier(nb)

if verif == True :
    print(nb,"""est bien un nombre premier.""")
else :
    print(nb,"""n'est pas un nombre premier.""")
        

L = int(input("""Quel est votre nombre de lignes ?"""))

ligne = 1

while ligne <= L :
    print("*"*ligne)
    ligne = ligne + 1

ligne = 1
espace = L

while ligne <= L :
    print(" "*(espace-1),"*"*ligne)
    ligne = ligne + 1
    espace = espace - 1

ligne = 1
espace = L
etoile = L
    
while ligne <= L :
    print("*"*etoile)
    ligne = ligne + 1
    etoile = etoile - 1


ligne = 1
espace = 0
etoile = L

while ligne <= L :
    print(" "*espace,"*"*etoile)
    ligne = ligne + 1
    espace = espace + 1
    etoile = etoile - 1


ligne = 1
espace = L-2
etoile = 1

while ligne <= L :
    if ligne == L :
        print("*"*(etoile//2),"o","*"*(etoile//2),sep='')
        break
    print(" "*espace,"*"*etoile)
    ligne = ligne + 1
    espace = espace - 1
    etoile = etoile + 2



entier= int(input("""Quel est votre entier ?"""))
ent = []
croissante = True
decroissante = True
constante = True
if entier >= 0:
    ent.append(entier)

while entier >= 0 :
    entier= int(input("""Quel est votre entier ?"""))
    if entier < 0 :
        break
    ent.append(entier)

index = 0
index2 = 1
while index <= len(ent)-1 and index2<= len(ent)-1 :
    if ent[index] >= ent[index2] :
        croissante = False
    if ent[index] <= ent[index2] :
        decroissante =  False
    if ent[index] < ent[index2] or ent[index] > ent[index+1]:
        constante = False
    index = index + 1
    index2 = index2 + 1

if croissante ==  True :
    print("""Votre suite est croissante.""")
elif decroissante == True :
    print("""Votre suite est décroissante.""")
elif constante ==  True :
    print("""Votre suite est constante.""")
else:
    print("""Votre suite n'est ni croissante, ni décroissante, ni constante.""")

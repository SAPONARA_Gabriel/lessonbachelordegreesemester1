def ajoute(mot1, mot2, d):
    if not(mot1 in d) :
        d[mot1] = mot2
    return d


def affiche(d):
    for mot in d :
        print(mot,"""<->""",d[mot])


def supprime(car, d):
    cle = list(d.keys())
    index = 0
    while index <= len(cle)-1:
        indice = cle[index]
        if indice[0] == car :
            del d[cle[index]]
        index = index+1
    return d


if __name__=="__main__":
    dictionaire = {}
    print("""Ce programme vous permet de créer un dictionnaire français-anglais de 5 mots.""")
    nb_mot = 0
    dictionnaire = {}
    while nb_mot<5:
        mot=str(input("""Quel est votre mot s'il vous plaît ?"""))
        traduction=str(input("""Quelle est la traduction en anglais du mot que vous avez donné précédemment s'il vous plaît ?"""))
        dictionnaire[mot] = traduction
        nb_mot = nb_mot+1

    ajout_mot=str(input("""Voulez-vous ajouter un mot ? "oui" ou "non" """))
    if ajout_mot == "oui":
        mot_ajout=str(input("""Quel est votre mot à ajouter ?"""))
        trad=str(input("""Quelle est la traduction du mot que vous souhaiteriez ajouter ?"""))
        ajouter = ajoute(mot_ajout, trad, dictionnaire)
        affiche(dictionnaire)

    suppr_mot=str(input("""Voudriez-vous supprimer un ou plusiers mot(s) ? "oui" ou "non" """))
    if suppr_mot == "oui":
        caractere=str(input("""Quel est le caractere dont vous désiriez que les mots commençant par ce dernier soit supprimer ?"""))
        dictionnaire=supprime(caractere, dictionnaire)
        affiche(dictionnaire)
    
        
                  



def evaluer(p,x):
    val_pol=0
    for key in p:
        val_pol = val_pol + p[key]*x**key
    return val_pol


def somme_polynomes(p1,p2):
    somme_poly={}
    for key in p1:
        if key in p2:
            somme_poly[key]=p1[key]+p2[key]
        else :
            somme_poly[key]=p1[key]
    for key in p2:
        if not(key in p1):
            somme_poly[key]=p2[key]   
    return somme_poly


def produit_polynomes(p1,p2):
    produit_poly={}
    for key in p1:
        for keys in p2:
            puissance = key+keys
            if puissance in produit_poly :
                terme = produit_poly[puissance]
                produit_poly[puissance] = terme + p1[key]*p2[keys]
            else:
                produit_poly[puissance] = p1[key]*p2[keys]
    return produit_poly



if __name__=="__main__":
    dictionaire = {}
    print("""Ce programme vous permet de travailler avec des polynomes.""")
    polynome1 = {}
    nb_terme = 0
    while nb_terme<3:
        puissance=int(input("""Quelle est la puissance d'un des termes de votre polynome s'il vous plaît ?"""))
        coefficient=int(input("""Quel est le coefficient du terme que vous avez donné précédemment s'il vous plaît ?"""))
        polynome1[puissance] = coefficient
        nb_terme = nb_terme+1
    polynome2 = {}
    nb_terme = 0
    while nb_terme<3:
        puissance=int(input("""Quelle est la puissance d'un des termes de votre polynome s'il vous plaît ?"""))
        coefficient=int(input("""Quel est le coefficient du terme que vous avez donné précédemment s'il vous plaît ?"""))
        polynome2[puissance] = coefficient
        nb_terme = nb_terme+1


    x = int(input("""Quel est le nombre à évaluer avec le polynome ?"""))
    print(evaluer(polynome1,x))
    print(somme_polynomes(polynome1,polynome2))
    print(produit_polynomes(polynome1,polynome2))

nb_s = int(input("""Quel est votre nombre de secondes ?"""))

nb_j = 0
nb_h = 0
nb_m = 0

nb_j = nb_s//86400
nb_h = (nb_s%86400)//3600
nb_m = ((nb_s%86400)%3600)//60
nb_s2 = ((nb_s%86400)%3600)%60

print(nb_s,"""secondes, correspond à:""",nb_j,"""jours""",nb_h,"""heures""",nb_m,"""minutes""",nb_s2,"""secondes.""")

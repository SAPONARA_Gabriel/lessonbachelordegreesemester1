annee = int(input("""Quelle est votre année ?"""))

if annee%4==0:
    if annee%400==0:
        print(annee,"""est une année bissextile.""")
    elif annee%100==0:
        print(annee,"""n'est pas une année bissextile.""")
    else:
        print(annee,"""est une année bissextile.""")
else:
    print(annee,"""n'est pas une année bissextile.""")

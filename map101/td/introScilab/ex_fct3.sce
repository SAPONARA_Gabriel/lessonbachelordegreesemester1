exec("mes_fonctions.sci", -1);

t = linspace (0,10,101)
u = f3(t) .* cos(t)
v = f3(t) .* sin(t)                 //penser à mettre un point pour que l'ordinateur comprenne que l'on multiplie élément par élément (avant  "."*)
w = 10*(log (1./sqrt(u^2+v^2)-1))^3

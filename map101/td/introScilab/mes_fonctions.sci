// definition de la fonction f1 avec y = f1(x) = 1/(1+x^2) 
deff("y = f1(x)" , "y = (1) ./ (1 + x .^ 2)");
// definition de la fonction f2 avec y = f2(t) = (t+1)^2 
deff("y = f2(t)" , "y = (t+1) .^ 2");
// definition de la fonction f3 avec y = f3(t) = 1/(1 + exp((x/10)1/3)
deff ("y = f3(x)", "y = (1) ./1+ exp((x./10).^1./3)");
// definition de la fonction f4 avec y = f4(t) = log(x + sqrt(x^2-1))
deff ("y = f4(x)", "y = log(x + sqrt(x^2-1))");
// definition de la fonction f5 avec y = f5(t) = log(x + sqrt(x^2+1))
deff ("y = f5(x)", "y = log(x + sqrt(x^2+1))");

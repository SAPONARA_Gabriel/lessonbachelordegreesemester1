// chargement du contenu du fichier nomm´e mes_fonctions.sci 
exec("mes_fonctions.sci", -1);
// calcul de f1(x) pour x=0 x=0,5 x=1 ... x=9,5 et x=10 
x = 0:0.5:10 
y = f1(x) 
disp(y)
// calcul de f2(u) pour 100 valeurs de u entre -10 et 10 
u = linspace (-10,10,100) 
z = f2(u) 
disp(z)

// definition de la fonction f avec y = f(x) = 1/(1+x^2) 
deff("y = f(x)" , "y = 1 ./ (1 + x .^ 2)");
//----- utilisation de la fonction f ----
x = 0:0.5:10 // calcul de pour x=0 , x=0,5 ,
y = f(x) // x=1 ... x=9,5 et x=10 
disp(y)

t = linspace (-1,2,1000) // calcul de f(t) pour 1000 valeurs de t
z = f(t) // ´equir´eparties entre -1 et 2 
disp(z)

